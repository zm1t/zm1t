* Структура

#+ATTR_HTML: :width 300px
[[./static/celtictree.jpg]]

** Сайт
*** Структура Проекта
*** Треккер
*** ВиКи
*** Блог
** Контент
*** Программирование
**** Лекции
***** SICP 1лекция
***** Джордж Дайсон: рождение компьютера
*** Математика
*** ИБ
**** Видео
***** ИБ в перспективе: прошлое и будущее
***** RussiaToday_ Ричард Столлман - Технологии свободы
*** GNU/Linux
**** The Code: Story of Linux documentary (MULTiSUB)
**** История GNU/Linux "Revolution OS" (правильный перевод)
**** Джим Землин Чему сфера технологий научилась у Линуса Торвальдса Jim Zemlin
*** Игры
**** История
**** Новости 
***** Эксклюзив
*** Визуализации
**** A brief history of Bitcoin development
**** Linux Kernel Development, 1991-20170826
**** Evolution of emacs (Gource Visualization)
*** Арт
**** It's a Unix system!
**** HACKERMAN'S HACKING TUTORIALS - How To Hack Time
**** Moleman 2: Демосцена - Искусство алгоритмов (2012) (Русская озвучка - A.e.r.o.)
**** СамИздат
******* Скринкасты
**** Курсы
***** ИТ
***** ИБ
***** Сети
***** Scheme 
***** GNU/Linux
***** EMACS
***** Математика
*** Технологии
**** GNU/Linux
**** Guile
**** Haskell
**** ELISP
**** Asm
**** C
**** Javascript
**** HTML/CSS
*** SVG
** Ссылки
** Авторы
*** Косов Пётр Борисович aka b11111000000 <11111000000@email.com>
*** Fyodor Shchukin ( ° ? ° ) 
https://github.com/honix 
https://phuda.bandcamp.com 
https://fedor.artstation.com
*** Сергей Горлянский (counterintuitive self-indulgent overkill)
https://vk.com/doc-94385154_456628946


