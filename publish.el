(let ((bootstrap-file (concat user-emacs-directory "straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 3))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "http://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

  (straight-use-package 'use-package)
  (package-initialize)

(use-package org :ensure t :straight t)

(use-package htmlize :ensure t :straight t)

(setq org-publish-use-timestamps-flag nil
      user-full-name "Peter Kosov"
      user-mail-address "11111000000@email.com")

(require 'json)

(defun org-export-json (pub-dir)
  (interactive)
  (let* ((tree (org-element-parse-buffer 'object nil)))
    (org-element-map tree (append org-element-all-elements
                                  org-element-all-objects '(plain-text))
      (lambda (x)
        (if (org-element-property :parent x)
            (org-element-put-property x :parent "none"))
        (if (org-element-property :structure x)
            (org-element-put-property x :structure "none"))
        ))
    (write-region
     (json-encode tree)
     nil (concat pub-dir (file-name-nondirectory (buffer-file-name)) ".json"))))

(defun org-publish-json (_plist filename pub-dir)
  "Publish to JSON"
  (unless (file-directory-p pub-dir)
    (make-directory pub-dir t))
  (find-file filename)
  (org-mode)
  (message "Exporting to JSON: %s" filename)
  (org-export-json pub-dir))

(let ((proj-base (file-name-directory default-directory)))
  (setq org-publish-project-alist
        `(("tangle"
           :base-directory ,(concat proj-base "org")
           :recursive t
           :publishing-directory ,(concat proj-base  "public")
           :publishing-function org-babel-tangle-publish)
          ("site"
           :base-directory ,(concat proj-base "org")
           :exclude ".*"
           :recursive nil
           :include ("index.org")
           :publishing-directory ,(concat proj-base  "public")
           :publishing-function org-html-publish-to-html)
          ("json"
           :base-directory ,(concat proj-base "org")
           :recursive t
           :publishing-directory ,(concat proj-base  "public")
           :publishing-function org-publish-json)
          ("static"
           :base-directory ,(concat proj-base "org/static")
           :base-extension any
           :publishing-directory ,(concat proj-base "public/static")
           :publishing-function org-publish-attachment)
          )))
(org-publish-all t)
